// styles fontawesome
import "./../node_modules/@fortawesome/fontawesome-free/css/all.min.css";

// bootstrap & bootstrap vue
import BootstrapVue from "bootstrap-vue";
import "./../node_modules/bootstrap/dist/css/bootstrap.css";
import "./../node_modules/bootstrap-vue/dist/bootstrap-vue.css";

// global css file
import "@/assets/style.sass";

import components from "@/components/components";
import ToastService from "@/services/toast.service";

import { ValidationObserver, ValidationProvider, extend } from "vee-validate";
import * as rules from "vee-validate/dist/rules";

const MyPlugin = {
  install(Vue) {
    // Install VeeValidate rules and localization
    Object.keys(rules).forEach((rule) => {
      extend(rule, rules[rule]);
    });

    // Install VeeValidate components globally
    Vue.component("ValidationObserver", ValidationObserver);
    Vue.component("ValidationProvider", ValidationProvider);

    Vue.use(BootstrapVue);
    Vue.use(ToastService);

    /* Register all components */
    const nameComponentesLoaded = [];
    Object.keys(components).forEach((k) => {
      nameComponentesLoaded.push(k);
      Vue.component(k, components[k]);
    });
  },
};

export default MyPlugin;
