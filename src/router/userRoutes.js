export default [
  {
    path: "/signin",
    name: "Signin",
    meta: { requiresAuth: false },
    component: () =>
      /* webpackChunkName: "Signin" */ import("../views/Signin/index.vue"),
  },
  {
    path: "/signup",
    name: "Signup",
    meta: { requiresAuth: false },
    component: () =>
      /* webpackChunkName: "Signup" */ import("../views/Signup/index.vue"),
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    meta: { requiresAuth: true },
    component: () =>
      /* webpackChunkName: "Dashboard" */ import(
        "../views/Dashboard/index.vue"
      ),
  },
];
