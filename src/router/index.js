import Vue from "vue";
import VueRouter from "vue-router";

// custom routes
import userRoutes from "./userRoutes";

Vue.use(VueRouter);

const routes = [
  { path: "/", redirect: "/signin" },
  ...userRoutes,
  { path: "*", redirect: "/signin" },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const hasToken = localStorage.getItem("ECTK");
  if (to.meta.requiresAuth && !hasToken) {
    next("/signin");
  }
  next();
});

router.beforeResolve((to, from, next) => {
  next();
});

export default router;
