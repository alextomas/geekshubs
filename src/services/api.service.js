import axios from "axios";

axios.defaults.baseURL = "https://apitrello.herokuapp.com";

axios.interceptors.request.use(
  function(config) {
    const token = localStorage.getItem("ECTK");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  function(err) {
    return Promise.reject(err);
  }
);

export default axios;
