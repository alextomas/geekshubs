/* eslint-disable no-param-reassign */
import _Vue from "vue";

/**
 * @override
 */
class CustomToast extends _Vue {
  vm;

  /**
   * @override
   */
  show(body, options) {
    // Se eliminan todas las alertas anteriores (directamente limpiando el HTML generado)
    // para mostrar solo la última
    const alerts = document.getElementsByClassName("b-toast-danger");
    for (let index = 0; index < alerts.length; index++) {
      const element = alerts[index];
      element.innerHTML = "";
    }
    this.$bvToast.toast(`${body}`, options);
  }

  /**
   * @override
   */
  success(title, body, autoHideDelay = 6000) {
    const toastOptions = {
      title: `${title}`,
      variant: "success",
      toaster: "b-toaster-bottom-left",
      solid: true,
      autoHideDelay,
    };
    this.show(body, toastOptions);
  }

  /**
   * @override
   */
  error(title, body, autoHideDelay = 10000) {
    // const title = err.name;
    // const body = err.message;
    const toastOptions = {
      title: `${title}`,
      variant: "danger",
      toaster: "b-toaster-bottom-left",
      solid: true,
      autoHideDelay,
    };
    this.show(body, toastOptions);
  }
}

const ToastService = (Vue) => {
  Vue.prototype.$toast = new CustomToast(Vue);
};

export default ToastService;
