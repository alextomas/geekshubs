const components = {
  Modal() {
    return import(/* webpackChunkName: "Modal" */ "@/components/Modal");
  },
};

export default components;
