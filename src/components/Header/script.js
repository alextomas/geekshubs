import computed from "./_computed";
import data from "./_data";
import methods from "./_methods";

export default {
  name: "Header",
  computed,
  data,
  methods,
};
