export default {
  logout() {
    localStorage.clear();
    this.$router.replace("signin");
  },
};
