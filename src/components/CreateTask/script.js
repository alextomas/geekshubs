import computed from "./_computed";
import data from "./_data";
import methods from "./_methods";

export default {
  name: "CreateTask",
  props: {
    list: {
      type: Object,
      required: true,
    },
  },
  computed,
  data,
  methods,
};
