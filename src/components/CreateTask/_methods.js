import api from "@/services/api.service";

export default {
  async create() {
    try {
      const payload = {
        idlist: this.list.id,
        task: this.task,
      };
      await api.post("/tasks", payload);
      this.addNewTask = false;
      this.task = "";
      this.$emit("refresh");
    } catch (error) {
      console.error(error);
    }
  },
  getValidationState({ dirty, validated, valid = null }) {
    return dirty || validated ? valid : null;
  },
};
