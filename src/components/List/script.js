import computed from "./_computed";
import data from "./_data";
import methods from "./_methods";

import CreateTask from "@/components/CreateTask";
import Task from "@/components/Task";

export default {
  name: "List",
  props: {
    list: {
      type: Object,
      required: true,
    },
  },
  components: { CreateTask, Task },
  computed,
  data,
  methods,
  created() {
    this.fetchData();
  },
};
