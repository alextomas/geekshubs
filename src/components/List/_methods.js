import api from "@/services/api.service";
import _ from "lodash";

const optionsModal = {
  title: "Eliminar",
  size: "sm",
  buttonSize: "sm",
  okVariant: "danger",
  okTitle: "Sí, eliminar",
  cancelTitle: "Cancelar",
  footerClass: "p-2",
  hideHeaderClose: false,
  centered: true,
};

export default {
  /**
   * Obtenemos las tareas de una lista
   */
  async fetchData() {
    try {
      const res = await api.get(`/list/tasks/${this.list.id}`);
      this.tasks = _.orderBy(res.data, ["createdAt", "id"], ["asc", "asc"]);
    } catch (error) {
      this.$toast.error("Error!", error);
    }
  },

  /**
   * Obtenemos las tareas de una lista
   */
  async update() {
    try {
      await api.put(`/list/${this.list.id}`, { name: this.list.name });
      this.$toast.success("¡OK!", "Lista actualizada con éxito.");
      this.editList = false;
    } catch (error) {
      this.$toast.error("¡Error!", error);
    }
  },

  /**
   * Muestra modal para eliminar una lista
   */
  async showModalRemoveList() {
    this.$bvModal
      .msgBoxConfirm(
        `Eliminar la lista '${this.list.name}' definitivamente?`,
        optionsModal
      )
      .then(async () => {
        await api.delete(`/list/${this.list.id}`);
        this.$root.$emit("refreshLists");
      })
      .catch((error) => {
        this.$toast.error("¡Error!", error);
      });
  },

  /**
   * Muestra modal para eliminar una tarea
   * @param {Object} task Tarea a eliminar
   */
  showModalRemoveTask(task) {
    this.$bvModal
      .msgBoxConfirm(
        `Eliminar la tarea '${task.task}' definitivamente?`,
        optionsModal
      )
      .then(async () => {
        await api.delete(`/tasks/${task.id}`);
        this.fetchData();
      })
      .catch((error) => {
        this.$toast.error("¡Error!", error);
      });
  },
};
