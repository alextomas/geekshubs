export default {
  name: "Modal",
  props: {
    modalId: {
      type: String,
    },
  },
  /**
   * @override
   */
  data() {
    return {
      isShow: false,
    };
  },
  /**
   * @override
   */
  mounted() {
    this.isShow = !this.isShow;
    this.$root.$on("bv::modal::show", () => {
      this.isShow = !this.isShow;
    });
    this.$root.$on("bv::modal::hide", () => {
      this.isShow = !this.isShow;
    });
  },
};
