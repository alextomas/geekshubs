import api from "@/services/api.service";

export default {
  async create() {
    try {
      const payload = { name: this.name };
      await api.post("/list", payload);
      this.name = "";
      this.addNewList = false;
      this.$root.$emit("refreshLists");
    } catch (error) {
      console.error(error);
    }
  },
};
