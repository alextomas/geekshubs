import computed from "./_computed";
import data from "./_data";
import methods from "./_methods";

export default {
  name: "Task",
  props: {
    task: {
      type: Object,
      required: true,
    },
  },
  computed,
  data,
  methods,
  mounted() {
    // this.fetchData();
  },
};
