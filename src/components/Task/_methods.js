import api from "@/services/api.service";

export default {
  // async fetchData() {
  //   try {
  //     const res = await api.get("/lists");
  //     console.log(res);
  //   } catch (error) {
  //     console.error(error);
  //   }
  // },
  // async remove() {
  //   try {
  //     await api.delete(`/list/${this.list.id}`);
  //     this.$bvModal.hide("modalRemove");
  //     this.$root.$emit("refreshLists");
  //   } catch (error) {
  //     console.error(error);
  //   }
  // },
  editTask() {
    this.edit = !this.edit;
  },
  async update() {
    try {
      this.edit = !this.edit;
      console.log("click");
      console.log(this.task);
      const res = await api.put(`/tasks/${this.task.id}`, {
        task: this.task.task,
      });
      console.log(res);
    } catch (error) {
      console.error(error);
    }
  },
};
