import api from "@/services/api.service";

const methods = {
  async doLogin() {
    this.loading = true;
    try {
      const { data: token } = await api.post("/users/login", this.form);
      localStorage.setItem("ECTK", token);
      this.$router.replace({ name: "Dashboard" }).then(() => {
        this.$toast.success("OK", "Login correcto");
        this.loading = false;
      });
    } catch (error) {
      this.$toast.error("¡Error!", "Verifique las credenciales.");
    }
  },
  getValidationState({ dirty, validated, valid = null }) {
    return dirty || validated ? valid : null;
  },
};

export default methods;
