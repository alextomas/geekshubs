const data = () => {
  return {
    loading: false,
    form: {
      username: "",
      password: "",
    },
  };
};

export default data;
