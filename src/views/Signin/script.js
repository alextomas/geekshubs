import data from "./_data";
import methods from "./_methods";
import { validationMixin } from "vuelidate";
import { required, minLength } from "vuelidate/lib/validators";

export default {
  name: "Signin",
  mixins: [validationMixin],
  data,
  methods,
  mounted() {
    localStorage.clear();
  },
  validations: {
    form: {
      username: {
        required,
        minLength: minLength(3),
      },
      password: {
        required,
        minLength: minLength(3),
      },
    },
  },
};
