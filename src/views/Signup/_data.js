const data = () => {
  return {
    loading: false,
    form: {
      username: null,
      password: null,
    },
  };
};

export default data;
