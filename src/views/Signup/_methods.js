import api from "@/services/api.service";

const methods = {
  async doSignup() {
    this.loading = true;
    try {
      await api.post("/users", this.form);
      this.$router
        .push({ path: "signin", query: { signup: true } })
        .then(() => {
          this.$toast.success("OK", "Cuenta creada con éxito.");
          this.loading = false;
        });
    } catch (error) {
      this.$toast.error("Error", `Login error: ${error}`);
    }
  },
  getValidationState({ dirty, validated, valid = null }) {
    return dirty || validated ? valid : null;
  },
};

export default methods;
