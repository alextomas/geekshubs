import data from "./_data";
import methods from "./_methods";

export default {
  name: "Signup",
  data,
  methods,
};
