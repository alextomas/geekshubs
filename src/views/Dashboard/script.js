import data from "./_data";
import methods from "./_methods";

import CreateList from "@/components/CreateList";
import List from "@/components/List";

export default {
  name: "Dashboard",
  components: {
    List,
    CreateList,
  },
  data,
  methods,
  mounted() {
    this.$root.$on("refreshLists", this.fetchData);
    this.loading = true;
    this.fetchData();
  },
  destroy() {
    this.$root.$off("refreshLists");
  },
};
