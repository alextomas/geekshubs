import api from "@/services/api.service";
import _ from "lodash";

const methods = {
  async fetchData() {
    try {
      const res = await api.get("/list");
      this.loading = false;
      this.lists = _.orderBy(res.data, ["createdAt", "id"], ["asc", "asc"]);
    } catch (error) {
      this.$toast.error("¡Error!", error);
    }
  },
};

export default methods;
