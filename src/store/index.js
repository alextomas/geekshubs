import Vue from "vue";
import Vuex from "vuex";

// custom modules
import applicationStore from "./application/index";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    applicationStore,
  },
});
