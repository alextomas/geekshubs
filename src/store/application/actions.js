/* eslint-disable no-new */
export default {
  setMenuOpened({ commit }, status) {
    commit("setMenuOpened", status);
  },
};
